#include "Line.h"

Line *createLine(float xi, float yi, float xf, float yf) {
    Line *l = (Line*)malloc(sizeof(Line));
    l->x_init = xi;
    l->y_init = yi;
    l->x_end = xf;
    l->y_end = yf;
    return l;
}

int isHorizontal(Line* l){
    return l->y_init == l->y_end;
}

void deleteLine(Line **list, int i, int n) {
    int k;
    for (k = i; k < (n - 1); k++){
        list[k] = list[k+1];
    }
}