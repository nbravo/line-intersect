#ifndef LINE_INTERSECT_SLAB_H
#define LINE_INTERSECT_SLAB_H

#include "Line.h"
#include "File.h"
#include "../algorithms/InputOutput.h"

#include <stdlib.h>

/**
 * Estructura que representa un Slab en Distribution Sweep
 * @param file archivo con todas las lineas ordenadas por coordenada horizontal
 * @param cursor posicion del cursor de file donde empiezan las lineas que conforman el slab
 * @param cantidad de lineas que pertenecen al slab
 * @param min max minimo y maximo de coordenada horizontal que comprende el slab
 */
typedef struct {
    File *file;
    int cursor;
    int n;
    float min, max;
} Slab;

/**
 * Crea un slab, alocandolo en el heap
 * @param f archivo al que se asocia el slab
 * @param min minima coordenada horizontal que comprende el slab
 * @param max maxima coordenada horizontal que comprende el slab
 * @return puntero al slab que se crea
 */
Slab *createSlab(File *f, float min, float max);

/**
 * Genera M/B slabs desde el archivo "X" tomando "n" lineas desde la posicion del cursor "cursor"
 * @param X archivo de lineas ordenadas por la coordenada horizontal
 * @param cursor posicion del cursor desde donde se obtendran las lineas del archivo
 * @param n cantidad de lineas que contendran los M/B slabs
 * @return lista (de punteros) a los M/B slabs generados desde el archivo
 */
Slab **generateSlabs(File *X, int cursor, int n, float slab_min, float slab_max);

/**********************************************************************************
 * Funciones aplicadas solo a lineas horizontales
 **********************************************************************************/

/**
 * Obtiene la posicion del slab que contiene al extremo izquierdo de la linea "l"
 * @param l linea a obtener su slab izquierdo
 * @param slabs lista de slabs
 * @param n largo de la lista de slabs
 * @return posicion en la lista del slab que contiene el extremo izquierdo de la linea, -1 si no se encuentra
 */
int getLeftSlab(Line *l, Slab **slabs, int n);

/**
 * Obtiene la posicion del slab que contiene al extremo derecho de la linea "l"
 * @param l linea a obtener su slab derecho
 * @param slabs lista de slabs
 * @param i posicion desde donde se comienza a buscar
 * @param n largo de la lista de slabs
 * @return posicion en la lista del slab que contiene el extremo derecho de la linea, -1 si no se encuentra
 */
int getRightSlab(Line *l, Slab **slabs, int i, int n);

/**
 * Obtiene el segmento izquierdo de una linea con respecto a su slab izquierdo
 * Este segmento va desde el punto inicial de la linea horizontal hasta el final de su slab izquierdo
 * @param slab slab izquierdo de la linea
 * @param line linea de donde obtener su segmento izquierdo
 * @return puntero (a Line) del segmento izquierdo de linea
 */
Line *getLeftSeg(Slab *slab, Line *line);

/**
 * Obtiene el segmento derecho de una linea con respecto a su slab derecho
 * Este segmento va desde el inicio del slab derecho hasta el punto final de la linea horizontal
 * @param slab slab derecho de la linea
 * @param line linea de donde obtener su segmento derecho
 * @return puntero (a Line) del segmento derecho de linea
 */
Line *getRightSeg(Slab *slab, Line *line);

#endif
