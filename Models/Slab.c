#include "Slab.h"

Slab *createSlab(File *f, float min, float max) {
    Slab *slab = (Slab*)malloc(sizeof(Slab));
    slab->file = f;
    slab->n = 0;
    slab->min = min;
    slab->max = max;
    return slab;
}

Slab **generateSlabs(File *X, int cursor, int n, float slab_min, float slab_max) { //TODO se esta generando mal el maximo del ultimo
    int i, j, k, width, reads = 0, tail;
    float min, max;
    Line **lines;
    Slab **slabs;

    //Se calcula el ancho de los slabs y se inicializan los slabs
    width = n / MB + ((n % MB == 0) ? 0: 1);
    slabs = (Slab**) malloc(MB * sizeof(Slab*));

    //Se prepara el archivo para ser leido
    X->cursor = cursor;
    X->total_lines_read = 0;

    //Se obtiene el inicio del primer slab
    lines = readLines(X);
    reads++;
    min = slab_min;
    if (slab_min != -1)
        min = lines[0]->x_init;
    slabs[0] = createSlab(X, min, 0);
    slabs[0]->cursor = cursor;

    //Se avanza hasta el final del primer slab
    while (X->total_lines_read < width) {

        //Liberacion de ultimas lineas leidas
        for (k = 0; k < X->n_last_read; k++)
            free(lines[k]);
        free(lines);

        lines = readLines(X);
        reads++;
    }

    tail = (X->total_lines_read - width);
    if (tail < 0)
        tail = 0;

    //Se genera el final del primer slab
    max =lines[X->n_last_read - tail - 1]->x_end;
    if (max < 0) {
        exit(4);
    }
    slabs[0]->max = max;
    slabs[0]->n = X->total_lines_read - tail;

    //Se repite el proceso de forma iterativa para todos los (M/B-1) slabs restates
    for (i = 1; i < MB; i++) {
        min = max;
        slabs[i] = createSlab(X, min, 0);
        slabs[i]->cursor = X->cursor - tail*L;
        X->total_lines_read = tail;
        if (X->n_last_read > width) {
            for (j = slabs[i-1]->n; j < X->n_last_read; j++){
                if (max < lines[j]->x_end)
                    max = lines[j]->x_end;
            }
        }
        while (X->total_lines_read < width) {

            //Se libera el espacio de las ultimas lineas borradas
            for (k = X->n_last_read - 1; k >= 0; k--)
                free(lines[k]);
            free(lines);

            lines = readLines(X);
            if (lines == NULL)
                break;
            reads++;
            for (j = 0; j < X->n_last_read; j++){
                if (max < lines[j]->x_end)
                    max = lines[j]->x_end;
            }
        }
        slabs[i]->max = max;
        tail = X->total_lines_read - width;
        if (tail < 0)
            tail = 0;
        slabs[i]->n = X->total_lines_read - tail;
    }
    if (slab_max != -1)
        slabs[MB - 1]->max = slab_max;

    //Ultima liberacion de memoria de las lineas leidas
    for (k = 0; k < X->n_last_read; k++)
        free(lines[k]);
    free(lines);

    return slabs;
}

int getLeftSlab(Line *l, Slab **slabs, int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (l->x_init >= slabs[i]->min && l->x_init <= slabs[i]->max)
            return i;
    }
    exit(2);
}

int getRightSlab(Line *l, Slab **slabs, int i, int n) {
    int j;
    for (j = i; j < n; j++) {
        if (l->x_end >= slabs[j]->min && l->x_end <= slabs[j]->max)
            return j;
    }
    exit(3);
}

Line *getLeftSeg(Slab* slab, Line *line){
    return createLine(line->x_init, line->y_init, slab->max, line->y_end);
}

Line *getRightSeg(Slab* slab, Line *line){
    return createLine(slab->min, line->y_init, line->x_end, line->y_end);
}
