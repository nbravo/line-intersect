#ifndef LINE_INTERSECT_FILE_H
#define LINE_INTERSECT_FILE_H

#include "Constants.h"

#include <stdlib.h>
#include <string.h>

/**
 * Estructura que representa un archivo de lineas.
 * @param path path del archivo
 * @param total_linea_read cantidad total de lineas leidas desde el primer read del archivo
 * @param cursor posicion donde quedo cursor en la ultima lectura. 0 si no se ha leido.
 * @param buffer buffer auxiliar para guardar parte de una linea si es que una lectura termino a a mitad de una linea
 * @param n_buffer cantidad de bytes ocupados en el buffer auxiliar
 * @param n_last_read cantidad de lineas Line leidas en la ultima lectura
 */
typedef struct {
    char *path;
    int cursor;
    int n_last_read;
    int total_lines_read;
    char buffer[L];
    int n_buffer;
} File;

/**
 * Crea un File, alocandolo en el heap
 * @param path path del archivo
 * @return puntero al File creado
 */
File *createFile(char *path);

/**
 * Destruye un File, liberandolo del heap
 * @param f puntero al File a destruir
 */
void destroyFile(File *f);

#endif