#ifndef LINE_INTERSECT_CONSTANTS_H
#define LINE_INTERSECT_CONSTANTS_H

#define M 10240
#define B 512
#define L (6*sizeof(char) + 4*sizeof(float))
#define MB ((M/B) + ((M % B) == 0 ? 0 : 1))
#define BL ((B/L) + ((B % L) == 0 ? 0 : 1))
#define EPSILON (1.0e-3)

#endif
