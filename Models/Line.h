#ifndef LINE_INTERSECT_LINE_H
#define LINE_INTERSECT_LINE_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/**
 * Estructura que representa una linea.
 * Una linea tiene punto inicial y punto final.
 * Para una linea horizontal, su punto inicial es el izquierdo y el final es el derecho.
 * Para una linea vertical, su punto inicial es el superios y el final es el inferior.
 * @param x_init: coordenada x inicial
 * @param y_init: coordenada y inicial
 * @param x_end: coordenada x final
 * @param y_end: coordenada y final
 */
typedef struct {
    float x_init, y_init, x_end, y_end;
} Line;

/**
 * Crea una linea, alocandola en el heap
 * @param xi coordenada x inicial
 * @param yi coordenada y inicial
 * @param xf coordenada x final
 * @param yf coordenada y final
 * @return puntero a una estrcutura Line
 */
Line *createLine(float xi, float yi, float xf, float yf);

/**
 * Chequea si una linea es horizontal
 * @param l puntero a una Line
 * @return 1 si la linea es horizontal, 0 si es vertical
 */
int isHorizontal(Line* l);

/**
 * Borra de la lista de lineas "list" de largo "n" la linea en la posicion "i".
 * Desplaza en una posicion menos los punteros desde "i+1" hasta "n-1".
 * No libera del heap la linea eliminada de la lista.
 * @param list lista (de punteros) de lineas
 * @param i posicion de la linea a eliminar
 * @param n largo total de la lista
 */
void deleteLine(Line **list, int i, int n);

#endif