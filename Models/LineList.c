#include "LineList.h"

LineList *createLineList(char *path) {
    LineList *list = (LineList*) malloc(sizeof(LineList));
    list->file = createFile(path);
    list->lines = NULL;
    list->i = 0;
    list->n = 0;
    return list;
}

void destroyLineList(LineList *l, int also_lines, int all_lines) {
    int i;
    destroyFile(l->file);
    if (also_lines) {
        if (l->lines) {
            if (all_lines) {
                for (i = 0; i < l->i; i++)
                    free(l->lines[i]);
            }
            //free(l->lines);
        }
    }
    free(l);
}

void addLineToList(LineList *list, Line *line) {
    int i;

    //Se verifica que su lista esta vacia
    if (list->lines == NULL) {
        list->lines = (Line **) malloc((B/L)*sizeof(Line*));
    }

    //Se agrega la linea a la lista, generando una copia de ella para evitar punteros locos
    // (ya que puede estar en varias otras listas)
    list->lines[list->i] = createLine(line->x_init, line->y_init, line->x_end, line->y_end);
    list->i++;
    list->n++;

    //Si esta lleno entonces se debe escribir en disco la lista y vaciarla
    if (list->i >= B/L) {
        writeLines(list->file, list->lines, list->i);
        for (i = 0; i < list->i; i++)
            free(list->lines[i]);
        free(list->lines);
        list->lines = NULL;
        list->i = 0;
    }
}