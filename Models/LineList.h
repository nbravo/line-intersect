#ifndef LINE_INTERSECT_LINELIST_H
#define LINE_INTERSECT_LINELIST_H

#include "File.h"
#include "Line.h"
#include "../algorithms/InputOutput.h"

#include <stdlib.h>

/**
 * Lista de lineas con un archivo donde guarda su contenido si su largo excede B/L (o su numero de bytes excede
 * un bloque)
 * @param file archivo donde guarda el contenido
 * @param lines lista de lineas
 * @param i largo de la lista "lines"
 * @param n cantidad de lineas total agregadas en la lista
 */
typedef struct {
    File *file;
    Line **lines;
    int i, n;
} LineList;

/**
 * Crea una LineList con un archivo de ruta "path", alocandolo en memoria
 * @param path ruta del archivo asociado a la LineList
 * @return puntero a la LineList creada
 */
LineList *createLineList(char *path);

/**
 * Destruye un LineList y su interior, liberando memoria del heap que utiliza
 * @param l LineList a liberar
 * @param also_lines 0 si no se quiere eliminar la lista lines, 1 si es que si se quiere
 * @param all_lines 0 si no se desea eliminar las lines dentro de la lista lines, 1 si es que si
 */
void destroyLineList(LineList *l, int also_lines, int all_lines);

/**
 * Agrega una linea a una LineList. Guarda la lista de la LineList en su archivo si luego de agregar la linea
 * el largo llega a B/L, el maximo permitido.
 * @param list LineList a la cual se le agrega la linea
 * @param line linea a agregar
 */
void addLineToList(LineList *list, Line *line);

#endif
