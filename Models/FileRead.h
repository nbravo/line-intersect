#ifndef LINE_INTERSECT_FILEREAD_H
#define LINE_INTERSECT_FILEREAD_H

#include "File.h"

#include <stdlib.h>
/**
 * Struct that contains a list of files and the number of files
 * @param list of files
 * @param number of files
 * @param number of lines
 */
typedef struct {
    File **list;
    int numberOfFiles;
    int numberOfLines;
} FileRead;

/**
 * Create a fileRead structure
 * @param l pointer to pointers of files
 * @param n number of files
 * @param m number of lines
 * @return FileRead structure.
 */
FileRead* createFileRead(File** l, int n, int m);
#endif
