#include "File.h"

File *createFile(char *path) {
    File *f = (File*)malloc(sizeof(File));
    f->path = (char*)malloc((strlen(path) + 1)*sizeof(char));
    f->path = strcpy(f->path, path);
    f->path[strlen(path)] = '\0';
    f->n_buffer = 0;
    f->cursor = 0;
    f->total_lines_read = 0;
    return f;
}

void destroyFile(File *f) {
    free(f->path);
    free(f);
}