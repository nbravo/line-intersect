#include <time.h>

#include "Models/File.h"
#include "algorithms/FileGenerator.h"
#include "algorithms/DistributionSweep.h"
#include "algorithms/MergeProc.h"


File *generateUniformExample(int size);
File *generateGaussianExample(int size);
int distSweep(File *X, File *Y, int n);
void generateSortAndSweep(int N, int dist);

int main() {
    generateSortAndSweep(512, 0);
    return 0;
}

/**
 * Inicia el proceso
 * @param n cantidad de lineas
 * @param dist distribucion de los datos. 0 es uniforme, 1 es normal
 */
void generateSortAndSweep(int N, int dist){
    clock_t beginX, endX, beginY, endY, beginds, endds;
    char *nameX = "mergedX.txt", *nameY = "mergedY.txt";
    File *X_file, *Y_file;
    FileRead *res, *rey;

    if (dist == 0){
        generateUniformExample(N);
    }
    else if (dist == 1){
        generateGaussianExample(N);
    }
    else {
        printf("Segundo parametro debe ser 0 o 1");
        exit(1);
    }

    printf("RESULTADOS MERGESORT\n");

    beginX = clock();
    res = createRuns("dummy.txt", "x");
    X_file = mergeRuns(res, "x", nameX, nameY);
    endX = clock();

    beginY = clock();
    rey = createRuns("dummy.txt", "y");
    Y_file = mergeRuns(rey, "y", nameX, nameY);
    endY = clock();
    reportIO();
    remove("Files/dummy.txt");
    printf("Tiempo total generacion y ordenacion [s] %f\n", (double) (endX - beginX)/CLOCKS_PER_SEC + (endY - beginY)/CLOCKS_PER_SEC);
    printf("--------------------------------------\n");


    printf("RESULTADOS DISTRIBUTION SWEEP\n");


    beginds = clock();
    distSweep(X_file, Y_file, N);
    endds = clock();
    remove(nameX);
    remove(nameY);
    system("exec rm Files/LineLists/*");
    remove("dummy.txt");
    printf("Tiempo total distribution sweep [s] %f\n", (double) (endds-beginds)/CLOCKS_PER_SEC);
}

/**
 * Funcion de distribution sweep
 * @param X archivo de lineas ordenadas por X
 * @param Y archivo de lineas ordenadas por Y
 * @param n cantidad de lineas
 * @return 0 si sale bien
 */
int distSweep(File *X, File *Y, int N) {
    distributionSweep(X,Y, N);
    free(X->path);
    free(Y->path);
    free(X);
    free(Y);
    return 0;
}

/**
 * Genera un archivo de lineas aleatorias con distribucion uniforme
 */
File *generateUniformExample(int size) {
    float l = 0.0;
    float h = 1000.0;
    return generateFileUniform(l, h, "dummy.txt", size, 0.5);
}

/**
 * Genera un archivo de lineas aleatorias con distribucion normal
 */
File *generateGaussianExample(int size) {
    return generateFileGaussian(250, 500, "dummy.txt", size, 0.25);
}