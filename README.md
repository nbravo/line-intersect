# line-intersect
Implementación junto a [Javier Espinoza](https://github.com/jaevespinoza)
## Descripción
Tarea 1 del curso "Diseño y Análisis de Algoritmos" del DCC, Universidad de Chile. Otoño, 2017.

Consiste en la implementación en C99 del algoritmo de memoria secundaria "Distribution Sweep" para encontrar intersecciones en un set de lineas
horizontales y verticales.

[Paper sobre el algoritmo](http://www.ics.uci.edu/~goodrich/pubs/ecg.pdf)
