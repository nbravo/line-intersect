#ifndef LINE_INTERSECT_FILEGENERATOR_H
#define LINE_INTERSECT_FILEGENERATOR_H

#include "../Models/File.h"
#include "InputOutput.h"

#include <math.h>
/**
 * Function that returns a random number between min and max
 */
float randr(float min, float max);
/**
 * Function that returns a gaussian distribution between 0 and 1
 * @return a float number between 0 and 1
 */
float gaussianForOne();

/**
 * Function that returns a number in a gaussian distribution with a mean and standard deviation
 * @param mean
 * @param standard
 * @return a number in a gaussian distribution
 */
float gaussian(float mean, float standard);

/**
 * Generates a file with Lines where the values are distributed in a gaussian distribution
 * @param standard deviation given
 * @param medium given to work
 * @param path Char where the file is written
 * @param full How many lines are written in total
 * @param alpha Charge of the lines written
 * @return File structure pointing to the file.
 */
File *generateFileGaussian(float standard, float medium, char *path, int full, double alpha);

/**
 * Generates a file with Lines where the values are distributed in a uniform distribution
 * @param low minimum value
 * @param high maximum value
 * @param path where the file is written
 * @param full How many lines are written in total
 * @param alpha charge of the lines written
 * @return File structure pointing to file
 */
File *generateFileUniform(float low, float high, char *path, int full, double alpha);
#endif
