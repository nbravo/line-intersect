#ifndef LINE_INTERSECT_DISTRIBUTIONSORT_H
#define LINE_INTERSECT_DISTRIBUTIONSORT_H

#include "InputOutput.h"
#include "../Models/Constants.h"
#include "../Models/File.h"
#include "../Models/LineList.h"
#include "../Models/Slab.h"

#include <stdlib.h>
#include <memory.h>

/**
 * Llamada inicial del algoritmo Distribution Sweep
 * Prepara los slabs iniciales para las siguientes llamadas recursivas
 * @param X archivo de lineas ordenado previamente por la coordendada horizontal
 * @param Y archivo de lineas ordenadi previamente por la coordendada vertical
 * @param n cantidad de lineas del input
 */
void distributionSweep(File *X, File *Y, int n);

/**
 * Llamada recursiva del algoritmo Ditribution Sweep
 * @param slabs slabs iniciales de esta recursion
 * @param vertical_recursion archivo de lineas de la recursion actual, ordenadas por la coordenada vertical
 * @param n cantidad de lineas de Y que caen en esta recursion
 */
void distributionSweepRec(Slab **slabs, LineList *vertical_recursion, int n);

/**
 * Reporta las intersecciones de una linea horizontal con todas las activas verticales
 * de un slab intermedio de "l"
 * @param active_verticals lineas verticales activas de un slab intermedio de "l"
 * @param l linea horizontal de la cual reportar intersecciones
 */
void reportIntersections(LineList *active_verticals, Line *l);

/**
 * Caso base de Distribution Sweep utilizando la fuerza bruta O(|lineas|^2)
 * Se llama cuando la cantidad de lineas de los slabs caen en memoria principal
 * @param lists lista de listas de lineas que seran tomadas en cuenta en el caso base
 * @param n_lists numero de listas
 * @param lengths largos de cada lista
 */
void baseCase(Line ***lists, int n_lists, int *lengths);

/**
 * Fcunion "techo" de a/b personalizada
 * Su creacion es para evitar cargar toda la biblioteca <math.h> por una sola funcion
 * @param a numerador
 * @param b denominador
 * @return techo de a/b
 */
int myCeil(int a, int b);

#endif
