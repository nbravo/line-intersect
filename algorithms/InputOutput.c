#include "InputOutput.h"

static int total_reads = 0;
static int total_writes = 0;

Line **readLines(File *f) {
    Line *l, **lines;
    FILE *fd;
    char buffer[B], firstLine[L], *head;
    int read = 0, i, not_empty_check = 0;

    //Se rellena el buffer con 0s
    memset(buffer, 0, B);

    fd = fopen(f->path, "r");
    if (fd == NULL) return NULL;

    //Se setea el puntero donde quedo en la ultima lectura
    fseek(fd, f->cursor, SEEK_SET);

    //Caso archivo vacio, se resuelve problema con EOF, solo se usa cuando se llega a la cola del archivo
    if (fread(buffer, B, 1, fd) <= 0) {
        total_reads++;
        //Heuristica para saber si se leyeron o no bytes desde el archivo
        for (i = 0; i < 10; i++) {
            not_empty_check += buffer[i];
        }
        if (!not_empty_check) {
            f->n_last_read = 0;
            fclose(fd);
            return NULL;
        }
    }
    fclose(fd);

    //Se reserva espacio para la lista de lineas
    lines = (Line**) malloc(BL*sizeof(Line*));

    head = buffer;

    //Caso donde se comenzo a leer desde en medio de una linea
    if (!(buffer[0] == '(' && buffer[L-1] == '\n')) {
        memcpy(firstLine, f->buffer, (size_t) f->n_buffer);
        memcpy(firstLine + f->n_buffer, buffer, L - f->n_buffer);
        l = (Line*)malloc(sizeof(Line));
        memcpy(&(l->x_init), firstLine + 1, sizeof(float));
        memcpy(&(l->y_init), firstLine + 6, sizeof(float));
        memcpy(&(l->x_end), firstLine + 11, sizeof(float));
        memcpy(&(l->y_end), firstLine + 16, sizeof(float));

        lines[read] = l;
        read++;
        head = buffer + (L - f->n_buffer);
    }

    //Priceso iterativo para crear una Line desde cada linea leida
    while (1) {
        if (!(head[0] == '(' && head[L-1] == '\n')){
            if ((f->n_buffer = B - L*read + f->n_buffer) >= 22)
                f->n_buffer = 0;
            memcpy(f->buffer, head, (size_t) f->n_buffer);
            break;
        }

        l = (Line*) malloc(sizeof(Line));
        memcpy(&(l->x_init), head + 1, sizeof(float));
        memcpy(&(l->y_init), head + 6, sizeof(float));
        memcpy(&(l->x_end), head + 11, sizeof(float));
        memcpy(&(l->y_end), head + 16, sizeof(float));

        lines[read] = l;
        read++;
        head+=22;
    }

    //Se deja preparado para la proxima lectra
    f->n_last_read = read;
    if (read < BL)
        f->cursor += read*((int)L);
    else
        f->cursor += B;
    f->total_lines_read += read;

    return lines;
}

void writeLines(File *f, Line *lines[], int n){
    int i, j, tail, remaining = 0, not_empty_check = 0;
    char buffer[B], aux_buffer[L];
    Line *l;
    FILE *fd;

    //Se llena el buffer con 0s
    memset(buffer, 0, B);

    fd = fopen(f->path, "a");
    j = remaining;

    //Proceso iterativo para escribir linea por linea
    for (i = 0; i < n; i++) {
        l = lines[i];
        memcpy(buffer + j, "(", sizeof(char));
        memcpy(buffer + j + sizeof(char), &(l->x_init), sizeof(float));
        memcpy(buffer + j + sizeof(char) + sizeof(float), ",", sizeof(char));
        memcpy(buffer + j + 2*sizeof(char) + sizeof(float), &(l->y_init), sizeof(float));
        memcpy(buffer + j + 2*sizeof(char) + 2*sizeof(float), ",", sizeof(char));
        memcpy(buffer + j + 3*sizeof(char) + 2*sizeof(float), &(l->x_end), sizeof(float));
        memcpy(buffer + j + 3*sizeof(char) + 3*sizeof(float), ",", sizeof(char));
        memcpy(buffer + j + 4*sizeof(char) + 3*sizeof(float), &(l->y_end), sizeof(float));
        memcpy(buffer + j + 4*sizeof(char) + 4*sizeof(float), ")", sizeof(char));
        memcpy(buffer + j + 5*sizeof(char) + 4*sizeof(float), "\n", sizeof(char));
        j += L;
        tail = B - j;

        //Caso en que nos quedamos en medio de una linea cuando llegamos a B bytes para la escritura
        if (tail < L && i < n-1) {
            i += 1;
            l = lines[i];
            memcpy(aux_buffer, "(", sizeof(char));
            memcpy(aux_buffer + sizeof(char), &(l->x_init), sizeof(float));
            memcpy(aux_buffer + sizeof(char) + sizeof(float), ",", sizeof(char));
            memcpy(aux_buffer + 2*sizeof(char) + sizeof(float), &(l->y_init), sizeof(float));
            memcpy(aux_buffer + 2*sizeof(char) + 2*sizeof(float), ",", sizeof(char));
            memcpy(aux_buffer + 3*sizeof(char) + 2*sizeof(float), &(l->x_end), sizeof(float));
            memcpy(aux_buffer + 3*sizeof(char) + 3*sizeof(float), ",", sizeof(char));
            memcpy(aux_buffer + 4*sizeof(char) + 3*sizeof(float), &(l->y_end), sizeof(float));
            memcpy(aux_buffer + 4*sizeof(char) + 4*sizeof(float), ")", sizeof(char));
            memcpy(aux_buffer + 5*sizeof(char) + 4*sizeof(float), "\n", sizeof(char));
            memcpy(buffer + j, aux_buffer, (size_t) tail);

            //Se escrine B bytes en el archivo
            fwrite(&buffer, B, 1, fd);
            total_writes++;

            remaining = 0;
            memset(&buffer, 0, B);
            memcpy(buffer, aux_buffer + tail, (size_t) L - tail);
            j = L - tail;
        }
        else if (tail == 0) {
            //Se escriben B bytes en el archivo
            fwrite(&buffer, B, 1, fd);
            total_writes++;

            remaining = 0;
            memset(&buffer, 0, B);
            j = 0;
        }
        remaining += 1;
    }
    //Eurisitca para saber si nos queda alguna linea a escribir en el archivo
    for (i = 0; i < 10; i++){
        not_empty_check += buffer[i];
    }

    //Se escribe lo que queda
    if (not_empty_check) {
        fwrite(buffer, (size_t) j-1, 1,fd);
        fwrite("\n", 1, 1, fd);
        total_writes++;
   }

    fclose(fd);
}

void reportIO(){
    printf("Total reads: %d\n", total_reads);
    printf("Total writes: %d\n", total_writes);
    printf("Total I/O: %d\n", total_reads + total_writes);
    total_reads = 0;
    total_writes = 0;
}