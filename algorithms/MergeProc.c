/*
 * MergeProc.c
 *
 * Copyright 2017 Javier <javier@javierPC>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include "MergeProc.h"

long sz;

int LineAisLessThanB(Line *a, Line *b, char *coor) {
    if (strcmp(coor, "x") == 0) {
        if (a->x_init < b->x_init) return 1;
        else if (fabs(a->x_init - b->x_init) < EPSILON) {
            if (a->y_init > b->y_init) return 1;
            else return 0;
        } else return 0;
    } else if (strcmp(coor, "y") == 0) {
        if (a->y_init > b->y_init) return 1;
        else if (fabs(a->y_init - b->y_init) < EPSILON) {
            if (!isHorizontal(a) && isHorizontal(b)) return 1;
            else if (!isHorizontal(b) && isHorizontal(a)) return 0;
            else return LineAisLessThanB(a, b, "x");
        } else return 0;
    }
    return -1;
}

void merge(Line **arr, int left, int medium, int right, char *coor)
{
    int i, j, k;
    int n1 = medium - left + 1;
    int n2 =  right - medium;

    Line *Le[n1];
    Line *Ri[n2];

    for (i = 0; i < n1; i++)
        Le[i] = arr[left + i];
    for (j = 0; j < n2; j++)
        Ri[j] = arr[medium + 1+ j];

    i = 0;
    j = 0;
    k = left;
    while (i < n1 && j < n2)
    {
        if (strcmp("x", coor) == 0){
            if (Le[i]->x_init < Ri[j]->x_init){
                arr[k] = Le[i];
                i++;
            } else if (Le[i]->x_init == Ri[j]->x_init){
                if (Le[i]->y_init >= Ri[j]->y_init){
                    arr[k] = Le[i];
                    i++;
                } else {
                    arr[k] = Ri[j];
                    j++;
                }
            } else {
                arr[k] = Ri[j];
                j++;
            }
            k++;
        } else if (strcmp("y", coor) == 0){
            if (Le[i]->y_init > Ri[j]->y_init){
                arr[k] = Le[i];
                i++;
            } else if (Le[i]->y_init == Ri[j]->y_init) {
                if (!isHorizontal(Le[i]) && isHorizontal(Ri[j])) {
                    arr[k] = Le[i];
                    i++;
                } else if (isHorizontal(Le[i]) && !isHorizontal(Ri[j])) {
                    arr[k] = Ri[j];
                    j++;
                } else {
                    if (Le[i]->x_init < Ri[j]->x_init) {
                        arr[k] = Le[i];
                        i++;
                    } else {
                        arr[k] = Ri[j];
                        j++;
                    }
                }
            } else {
                arr[k] = Ri[j];
                j++;
            }
            k++;
        }
    }

    while (i < n1)
    {
        arr[k] = Le[i];
        i++;
        k++;
    }

    while (j < n2)
    {
        arr[k] = Ri[j];
        j++;
        k++;
    }
}

void mergeSortLine(Line **arr, int left, int right, char *coor)
{
    if (left < right)
    {
        int medium = left+(right-left)/2;

        mergeSortLine(arr, left, medium, coor);
        mergeSortLine(arr, medium+1, right, coor);
        merge(arr, left, medium, right, coor);
    }
}

long getSizeOfFile(File* f){
    FILE *fd = fopen(f->path, "r");
    fseek(fd, 0L, SEEK_END);
    long sz = ftell(fd);
    rewind(fd);
    return sz;
}

FileRead *createRuns(char *dir, char *coor)
{
    int k = 0, write = 0, lines = 0, indicador = 0, i = 0;
    FileRead* deliver;
    /* crear lista de lista de lineas */
    Line** listLine;
    /* buffer de nombre */
    char buffer[22];
    buffer[21] = '\0';

    /* crear lista de files */
    /* tener en cuenta el numero que multiplica a sizeof(File*). hay
     * que esperar que sea grande*/
    File *f = createFile(dir);
    sz = (getSizeOfFile(f)/B) + 1;
    File** listFile = (File**)malloc((sz+sz)*sizeof(File*));
    char *d;

    //mientras el leer no sea null
    while((listLine = readLines(f))) {
        // leer las lineas
        // Sortear las lineas por la coordenada puesta

        lines += f->n_last_read;
        mergeSortLine(listLine, 0, f->n_last_read - 1, coor);

        // Asignar nombre al archivo
        sprintf(buffer, "text_%d.txt", write);
        write++;
        d = &buffer[0];

        // crear el archivo necesario para poder escribir
        listFile[indicador] = createFile(d);

        // escribir en el archivo
        writeLines(listFile[indicador], listLine, f->n_last_read);

        //mover indicador
        indicador++;
    }
    free(f);
    free(listLine);
    deliver = createFileRead(listFile, write, lines);
    return deliver;
}

File *mergeRuns(FileRead* runs, char *coor, char* nameX, char* nameY) {

    int i = 0, write = 0, elegido = 0, totalLines = 0,
            k = 0, timesRead = 0, c = 0, passer, indicator = 0, number = 0,
            arrow = 0;
    int cmpstrx = strcmp(coor, "x");
    int cmpstry = strcmp(coor, "y");
    long fileSizes;

    Line *lineAggregate;

    char name;

    int numberLines[(sz + sz)];


    File *interFile;
    Line ***firstLines = (Line ***) malloc((sz + sz) * sizeof(Line **));
    // tamaño de archivos
    fileSizes = 0;
    // cantidad de veces leidas
    timesRead = 0;
    // total de lineas
    totalLines = 0;
    // indicador
    indicator = 0;
    for (passer = 0; passer < 2; passer++) {
        for (c = 0; c < runs->numberOfFiles; c++) {
            // leer el archivo

            firstLines[indicator] = readLines(runs->list[c]);
            if (!firstLines[indicator]) {
                if (c >= runs->numberOfFiles && passer < 0) break;
                else if (c < runs->numberOfFiles && passer > 0) continue;
                else if (c >= runs->numberOfFiles && passer > 0) break;
            }
            totalLines += runs->list[c]->n_last_read;
            numberLines[timesRead] = runs->list[c]->n_last_read;
            timesRead++;
            // si estoy leyendo por segunda vez
            if (passer == 0) {
                fileSizes += getSizeOfFile(runs->list[c]);
                number++;
            }
            indicator++;
        }
    }

    // mover flecha del archivo
    arrow = arrow + c;

    //tener lista de indices para cada archivo
    int indices[timesRead];
    // inicializar lista de indices
    for (i = 0; i < timesRead; i++)
        indices[i] = 0;

    //printf("totalLines %i, fileSize %lu, number %i\n", totalLines, fileSizes, number);
    Line **mLine = (Line **) malloc(totalLines * sizeof(Line **));
    for (i = 0; i < runs->numberOfLines; i++) {
        if (cmpstrx == 0) lineAggregate = createLine(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);
        else if (cmpstry == 0) lineAggregate = createLine(-FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX);
        for (k = 0; k < timesRead; k++) {
            //ver si el indice que tengo en el archivo es mayor o menor que el numero de
            //lineas en el archivo
            if (indices[k] < numberLines[k]) {
                if (LineAisLessThanB(firstLines[k][indices[k]], lineAggregate, coor)) {
                    lineAggregate = firstLines[k][indices[k]];
                    elegido = k;
                }
            }
        }
        //asignar linea a la lista
        mLine[i] = lineAggregate;
        /*printf("line %i to add %.2f, %.2f, %.2f, %.2f\n", i,
               mLine[i]->x_init, mLine[i]->y_init, mLine[i]->x_end,
               mLine[i]->y_end);*/

        //mover el indice del archivo de donde sale la linea
        indices[elegido]++;
    }
    if (strcmp(coor, "x") == 0)     interFile= createFile(nameX);
    else if (strcmp(coor, "y") == 0)interFile= createFile(nameY);
    writeLines(interFile, mLine, totalLines);
    //free(mLine);

    //remover los archivos
    for (i = 0; i < runs->numberOfFiles; i++)
        remove(runs->list[i]->path);
    return interFile;
}





