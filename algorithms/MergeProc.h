#ifndef LINE_INTERSECT_MERGE_H
#define LINE_INTERSECT_MERGE_H

#include "../Models/Constants.h"
#include "../Models/File.h"
#include "../Models/FileRead.h"
#include "../Models/Line.h"
#include "InputOutput.h"

#include <string.h>
#include <float.h>
#include <stdlib.h>
/**
 * Merge procedure
 * @param arr array to be sorted
 * @param left index
 * @param medium index
 * @param right index
 * @param coor coordenate to sort
 */
void merge(Line **arr, int left, int medium, int right, char *coor);

/**
 * Merge complete procedure
 * @param arr Array to be sorted
 * @param left index
 * @param right index
 * @param coor coordenate to sort
 */
void mergeSortLine(Line **arr, int left, int right, char *coor);

/**
 * Create the initial runs
 * @param dir path to file
 * @param coor coordenate to sort
 * @return FileRead structure pointing to the list of files
 */
FileRead* createRuns(char *dir, char *coor);
/**
 * Merge the initial runs given by createRuns
 * @param runs FileRead structure that contains the runs
 * @param coor coordenate to sort
 * @param nameX name of future x-sorted line file
 * @param nameY name of future y-sorted line file
 * @return Another FileRead
 */
File *mergeRuns(FileRead* runs, char *coor, char *nameX, char *nameY);

/**
 * Get size of File
 * @param f File structure
 * @return size of file f
 */
long getSizeOfFile(File* f);
/**
 * Compare 2 lines based on a coordenate
 * @param a line
 * @param b line
 * @param coor coordenate to compare
 * @return int regarding the comparation
 */
int LineAisLessThanB(Line *a, Line *b, char *coor);

#endif
