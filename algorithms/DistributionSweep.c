#include "DistributionSweep.h"

//Conteo de recursiones de recursion que se lleva y asi nombrar correctamente los archivos
//de los slabs de cada recursion
static int recursionCounter = 0;
static int intersections = 0;

void distributionSweep(File *X, File *Y, int n) {
    //int i;
    Slab **x;
    LineList *y;

    //Se crea una linelist a partir del archivo de lineas ordenadas por la coordenada vertical
    y = createLineList(Y->path);
    y->n = n;

    //Generacion de slabs iniciales
    x = generateSlabs(X, 0, n, -1, -1);

    //Llamada recursiva
    distributionSweepRec(x, y, n);

    //Se libera la memoria memoria de y (x ya se libero durante el proceso)
    destroyLineList(y, 1, 1);
    printf("Total intersections: %d\n", intersections);
    intersections = 0;
    reportIO();
}

void distributionSweepRec(Slab **slabs, LineList *vertical_recursion, int n) {
    //contadores
    int i, j, k, t;

    //largo de la lista de donde se obtiene la sweep_line, numero de listas en el caso base y
    // largo de las listas del caso base
    int sweep_line_limit, base_case_n_list, *base_case_lists_lengths;

    //buffer para crear los nombres de los archivos recursivos
    char name_buffer[80];

    //lista desde donde obtener las lineas que haran de sweep_line, la sweep_line,
    //segmento izquierdo y derecho de una linea horizontal a cortar,
    //lista de listas que estaran en el caso base
    Line **sweep_line_list = NULL, *sweep_line, *leftSeg, *rightSeg, ***base_case_list;

    //verticales activas actuales y las lineas a considerar en la siguiente recursion
    LineList **active_verticals, **slab_recursive_list;

    //Slabs que se tomaran en cuenta en la proxima recursion
    Slab **recursive_slabs;

    //Caso base |slabs| <= M
    if (n*L <= M) {
        //Caso en que no se encuentran lineas ordenadas verticalmente (de la recursion actual) en memoria principal
        //Se crean listas de lineas a partir de todos los readLines posible sobre el archivo de vertical_recursion
        //y luego se aplica el caso base sobre todas estas listas de lineas
        if (vertical_recursion->lines == NULL) {
            base_case_lists_lengths = (int*)malloc(myCeil(vertical_recursion->n*L, B)*sizeof(int));
            base_case_list = (Line***)malloc(myCeil(vertical_recursion->n*L, B)*sizeof(Line**));
            for (base_case_n_list = 0; base_case_n_list < myCeil(vertical_recursion->n*L, B); base_case_n_list++){
                base_case_list[base_case_n_list] = readLines(vertical_recursion->file);
                base_case_lists_lengths[base_case_n_list] = vertical_recursion->file->n_last_read;
            }
            baseCase(base_case_list, base_case_n_list, base_case_lists_lengths);
            for (i = 0; i < base_case_n_list; i++) {
                for(j = base_case_lists_lengths[i] - 1; j >= 0; j--)
                    free(base_case_list[i][j]);
                free(base_case_list[i]);
            }
            free(base_case_lists_lengths);
            free(base_case_list);
            return;
        }
        //Caso en que existen lineas en memoria principal, en cuyo caso se aplica el mismo procedimiento anterior
        //pero tomando en cuenta ademas la lista de lineas en memoria principal de vertical_recursion
        base_case_list = (Line***)malloc((myCeil((vertical_recursion->n - vertical_recursion->i)*L, B) + 1)*sizeof(Line**));
        base_case_lists_lengths = (int*)malloc((myCeil((vertical_recursion->n-vertical_recursion->i)*L,B) + 1)*sizeof(int));
        for (base_case_n_list = 0; base_case_n_list < myCeil((vertical_recursion->n-vertical_recursion->i)*L, B); base_case_n_list++){
            base_case_list[base_case_n_list] = readLines(vertical_recursion->file);
            base_case_lists_lengths[base_case_n_list] = vertical_recursion->file->n_last_read;
        }
        base_case_list[base_case_n_list] = vertical_recursion->lines;
        base_case_lists_lengths[base_case_n_list] = vertical_recursion->i;
        base_case_n_list++;
        baseCase(base_case_list, base_case_n_list, base_case_lists_lengths);
        /* free deseable, pero genera problema con otros free
        for (i = 0; i < (base_case_n_list - 1); i++) {
            for(j = base_case_lists_lengths[i] - 1; j >= 0; j--)
                free(base_case_list[i][j]);
            free(base_case_list[i]);
        }
         */
        free(base_case_lists_lengths);
        free(base_case_list);
        return;
    }

    //Se crean listas de lineas para guardar las lineas de cada recursion
    //en cada slab, ademas de la lista de activas verticales actuales
    active_verticals = (LineList**) malloc(MB*sizeof(LineList*));
    slab_recursive_list = (LineList**) malloc(MB*sizeof(LineList*));
    for (k = 0; k < MB; k++) {
        sprintf(name_buffer, "Files/LineLists/av_%d_%d.txt", recursionCounter, k);
        active_verticals[k] = createLineList(name_buffer);

        sprintf(name_buffer, "Files/LineLists/slab_rec_%d_%d.txt", recursionCounter, k);
        name_buffer[32] = '\0';
        slab_recursive_list[k] = createLineList(name_buffer);
    }

    //Primera obtencion de lineas desde vertical_recursion (ordendas por la coordenada vertical)
    if (vertical_recursion->i > 0) {
        sweep_line_list = vertical_recursion->lines;
        sweep_line_limit = vertical_recursion->i;
    }
    else {
        sweep_line_list = readLines(vertical_recursion->file);
        sweep_line_limit = vertical_recursion->file->n_last_read;
    }

    //Se hace el proceso iterativo de tomar cada linea y ver si es horizontal y vertical
    //moviendo la sweep_line segun el caso
    while (sweep_line_list != NULL) {
        for (k = 0; k < sweep_line_limit; k++) {
            sweep_line = sweep_line_list[k];

            //Agregar linea vertical al active_vertical_list correspondiente como tambien a su slab respectivo
            //para la siguiente iteracion
            if (!isHorizontal(sweep_line)) {
                i = getLeftSlab(sweep_line, slabs, MB);
                addLineToList(active_verticals[i], sweep_line);
                addLineToList(slab_recursive_list[i], sweep_line);//Se chequea sus slabs correspondientes y se parte entre ellos, ademas de rep
            }
                //Se chequea sus slabs correspondientes y se obtiene los segmentos izquierdo y derecho de la sweep_line
                //ademas de reportar lo puntos de los slabs intermedios
            else {
                i = getLeftSlab(sweep_line, slabs, MB);
                j = getRightSlab(sweep_line, slabs, i, MB);

                //caso en que la sweep_line cae en un solo slab
                if (i == j) {
                    addLineToList(slab_recursive_list[i], sweep_line);
                    continue;
                }

                //se reportan las intersecciones de las active_vertical de los slabs intermedios
                for (t = i + 1; t < j; t++) {
                    reportIntersections(active_verticals[t], sweep_line);
                }

                //Se parte la linea horizontal en sus segmentos izquierdo y derecho
                //y se agregan estos a los slabs correspondientes, para la proxima iteracion
                leftSeg = getLeftSeg(slabs[i], sweep_line);
                rightSeg = getRightSeg(slabs[j], sweep_line);

                addLineToList(slab_recursive_list[i], leftSeg);
                addLineToList(slab_recursive_list[j], rightSeg);
            }
            //free(sweep_line);
        }
        free(sweep_line_list);

        //Se actualiza la lista de sweep_lines, obteniendo B/L nuevas lineas
        sweep_line_list = readLines(vertical_recursion->file);
        sweep_line_limit = vertical_recursion->file->n_last_read;
    }

    //Se libera la memoria ocupada por cada lista de lineas verticales
    for (k = 0; k < MB; k++){
        destroyLineList(active_verticals[k], 1, 1);
    }
    free(active_verticals);

    //Se aumenta el conteo de recursiones
    recursionCounter++;
    //Llamado recurivo para cada slab
    for (t = 0; t < MB; t++) {

        //Se generan los slabs recursivos sobre el slab t
        recursive_slabs = generateSlabs(slabs[t]->file, slabs[t]->cursor, slabs[t]->n, slabs[t]->min, slabs[t]->max);

        //Llamado recursivo para el slab t
        distributionSweepRec(recursive_slabs, slab_recursive_list[t], slabs[t]->n);

        //Luego del llamado recursivo, se libera ese slab
        free(slabs[t]);
        destroyLineList(slab_recursive_list[t], 1, 0);
    }
    //Se libera las lista de slabs
    free(slab_recursive_list);
    free(slabs);
}

void reportIntersections(LineList *active_verticals, Line *l) {
    int i, n_lines_after;
    float y = l->y_init;
    Line **currentList;
    char *fileName;
    File *new_active_verticals;

    //archivo para guardar las nuevas listas de activas verticales
    //surgidas por las eliminaciones lazy
    fileName = (char*)malloc((strlen(active_verticals->file->path) + 1)*sizeof(char));
    strcpy(fileName, active_verticals->file->path);
    sprintf(fileName + strlen(fileName), "%d", recursionCounter + 1);
    new_active_verticals = createFile(fileName);

    i = 0;
    currentList = readLines(active_verticals->file);
    n_lines_after = active_verticals->file->n_last_read;
    while (currentList != NULL) {
        while(i < n_lines_after) {
            //Linea a borrar de manera lazy
            if (currentList[i]->y_end > y) {
                deleteLine(currentList, i, n_lines_after);
                n_lines_after--;
                continue;
            }
            //Se reporta la interseccion con una linea vertical
            printf("(%.3f, %.3f)\n", currentList[i]->x_init, y);
            i++;
            intersections++;
        }
        //Se guarda en un archivo la lista actual y se procede a la siguiente lista
        writeLines(new_active_verticals, currentList, n_lines_after);
        currentList = readLines(active_verticals->file);
        n_lines_after = active_verticals->file->n_last_read;
    }
    //Se actualiza los activos verticales con las nuevas listas de activas verticales
    remove(active_verticals->file->path);
    rename(new_active_verticals->path, active_verticals->file->path);
    destroyFile(new_active_verticals);

    //Se chequea finalmente con las lineas que se encontraban en memoria principal
    //Es seguro que estas son las ultimas active_verticals agregadas, por lo que
    //su verificacion en ultimo lugar es correcto
    currentList = active_verticals->lines;
    while (i < active_verticals->i) {
        //Linea a borrar de manera lazy
        if (currentList[i]->y_end > y) {
            deleteLine(currentList, i, active_verticals->i);
            active_verticals->i--;
            continue;
        }
        printf("(%.3f, %.3f)\n", currentList[i]->x_init, y);
        i++;
        intersections++;
    }
}

void baseCase(Line ***lists, int n_lists, int *lengths) {
    int active_vertical_n = 0, i, j, k, total_lines = 0;
    Line **current_list, **active_vertical, *l;

    //Se obtiene la cantidad de lineas total participando en el caso base
    for (k = 0; k < n_lists; k++) {
        total_lines += lengths[k];
    }

    //Activas verticales, una lista de las lineas verticales que se van encontrando
    active_vertical = (Line **) malloc(total_lines * sizeof(Line *));

    //Se itera sobre cada lista
    for (k = 0; k < n_lists; k++) {
        //Se obtiene una lista y se itera sobre ella
        current_list = lists[k];
        for (i = 0; i < lengths[k]; i++) {
            //Se obtiene una linea
            l = current_list[i];
            //Caso en que la linea es vertical, en cuyo caso se agrega a las active_vertical
            if (!isHorizontal(l)) {
                active_vertical[active_vertical_n] = l;
                active_vertical_n++;
            }
                //Caso en que se la linea es horizontal, en cuyo caso se procede de manera similar
                //a distribution_sweep
            else {
                //Se ve la interseccion con todas las active_verticals
                for (j = 0; j < active_vertical_n; j++) {
                    //Se elimina una linea vertical de manera lazy
                    if (active_vertical[j]->y_end > l->y_init) {
                        deleteLine(active_vertical, j, active_vertical_n);
                        active_vertical_n--;
                    } else {
                        //Se reporta una interseccion
                        if (l->x_init < active_vertical[j]->x_init && l->x_end > active_vertical[j]->x_init) {
                            printf("(%.3f, %.3f)\n", active_vertical[j]->x_init, l->y_init);
                            intersections++;
                        }

                    }
                }
            }

        }
    }
    //Se libera la lista de active_verticals, pero no las lineas ya que
    //puede que se utilizen en otra recursion
    free(active_vertical);
}

int myCeil(int a, int b){
    return a/b + ((a % b == 0) ? 0 : 1);
}

