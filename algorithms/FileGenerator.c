#include "FileGenerator.h"

float randr(float min, float max)
{
    if (min > max) return max;
    float scaled = (float)rand()/(float)(RAND_MAX);

    return (max - min +1)*scaled + min;
}

float gaussianForOne(){
    float r, x , y;
    x = randr(0.0, 1.0);
    y = randr(0.0, 1.0);
    r = x*x + y*y;
    return (x * sqrtf(fabsf(-2*logf(r)/r)));
}

float gaussian(float mean, float standard) {
    return roundf(mean + standard * gaussianForOne());
}

float uniform(float rangeLow, float rangeHigh) {
    float myRand = (float)rand()/(float)(RAND_MAX);
    float range = rangeHigh - rangeLow + 1;
    float scaled = (myRand * range) + rangeLow;
    float b = scaled;
    float p = roundf(b);
    return p;
}

File *generateFileUniform(float low, float high, char *path, int full, double alpha){
    int i, hor = 0;
    double horizontal;
    File *newFile = createFile(path);
    Line **linesToWrite;
    linesToWrite= (Line**)malloc(full*sizeof(Line*));
    for (i = 0; i < full; i++) {
        horizontal = (double)rand() / (double)RAND_MAX;
        if (horizontal > alpha) hor = 0;
        else hor = 1;
        linesToWrite[i] = (Line *) malloc(sizeof(Line));
        linesToWrite[i]->x_init = uniform(low, high);
        linesToWrite[i]->y_init = uniform(low, high);
        if (hor) {
            linesToWrite[i]->x_end = uniform(low, high) + linesToWrite[i]->x_init;
            linesToWrite[i]->y_end = linesToWrite[i]->y_init;
        } else {
            linesToWrite[i]->x_end = linesToWrite[i]->x_init;
            linesToWrite[i]->y_end = fabsf(uniform(low, high) - linesToWrite[i]->y_init);
            if (linesToWrite[i]->y_end > linesToWrite[i]->y_init){
                float temp = linesToWrite[i]->y_end;
                linesToWrite[i]->y_end = linesToWrite[i]->y_init;
                linesToWrite[i]->y_init = temp;
            } else if (linesToWrite[i]->y_end == linesToWrite[i]->y_init){
                linesToWrite[i]->y_init += 2;
            }
        }
    }
    writeLines(newFile, linesToWrite, full);
    for (i = 0; i < full; i++) {
        free(linesToWrite[i]);
    }
    free(linesToWrite);
    return newFile;
}


File *generateFileGaussian(float standard, float medium, char *path, int full, double alpha){
    int i, hor = 0;
    double horizontal;
    File *newFile = createFile(path);
    Line **linesToWrite = (Line**)malloc(full*sizeof(Line*));
    for (i = 0; i < full; i++){
        horizontal = (double)rand() / (double)RAND_MAX;
        if (horizontal > alpha) hor = 0;
        else hor = 1;
        linesToWrite[i] = (Line*) malloc(sizeof(Line));
        linesToWrite[i]->x_init = gaussian(medium, standard);
        linesToWrite[i]->y_init = gaussian(medium, standard);
        if (hor) {
            linesToWrite[i]->x_end = gaussian(medium, standard) + linesToWrite[i]->x_init;
            linesToWrite[i]->y_end = linesToWrite[i]->y_init;
        } else{
            linesToWrite[i]->x_end = linesToWrite[i]->x_init;
            linesToWrite[i]->y_end = (float)(fabs(gaussian(medium, standard) - linesToWrite[i]->y_init));
            if (linesToWrite[i]->y_end >= linesToWrite[i]->y_init){
                float temp = linesToWrite[i]->y_end;
                linesToWrite[i]->y_end = linesToWrite[i]->y_init;
                linesToWrite[i]->y_init = temp;
            }
        }
    }
    writeLines(newFile, linesToWrite, full);
    for (i = 0; i < full; i++) {
        free(linesToWrite[i]);
    }
    free(linesToWrite);
    return newFile;
}
