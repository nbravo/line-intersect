#ifndef LINE_INTERSECT_INPUTOUTPUT_H
#define LINE_INTERSECT_INPUTOUTPUT_H

#include "../Models/Constants.h"
#include "../Models/File.h"
#include "../Models/Line.h"

#include <stdlib.h>
#include <string.h>

/**
 * Lee B bytes desde el archivo "f" y se entrega una lista de Line de largo maximo B/L
 * @param f archivo desde donde leer
 * @return lista de lineas Line leidas desde el archivo, de largo maximo B/L
 */
Line **readLines(File *f);

/**
 * Escribe la lista de lineas "lines" de largo n, en la cola del archivo "f"
 * @param f archivo sobre el cual se escribira la lista
 * @param lines lista de lineas a escribir
 * @param n largo de "lines"
 */
void writeLines(File* f, Line *lines[], int n);

/**
 * Reporta en la salida estandar la cantidad de IO que se generaron hasta ahora
 */
void reportIO();

#endif
